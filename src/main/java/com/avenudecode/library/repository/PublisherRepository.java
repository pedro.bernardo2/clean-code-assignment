package com.avenudecode.library.repository;

import com.avenudecode.library.entity.Publisher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PublisherRepository extends JpaRepository<Publisher, String> {

    @Query("SELECT p FROM Publisher p WHERE p.name LIKE :name")
    public List<Publisher> getPublisherByName(String name);

    @Query("SELECT p FROM Publisher p " +
            "INNER JOIN Book b " +
            "WHERE b.publisher.id = p.id " +
            "AND b.id = :id")
    public List<Publisher> getPublisherByBook(String id);
}
