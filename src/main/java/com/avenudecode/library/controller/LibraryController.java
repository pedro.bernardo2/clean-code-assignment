package com.avenudecode.library.controller;

import java.util.List;

import com.avenudecode.library.entity.Publisher;
import com.avenudecode.library.service.BookService;
import com.avenudecode.library.service.PublisherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.avenudecode.library.entity.Book;

@RestController
@RequestMapping("/library")
public class LibraryController {
	
	@Autowired
	private BookService bookService;

	@Autowired
	private PublisherService publisherService;
	
	@GetMapping("/book")
	public List<Book> getAllBooks() { return bookService.getAllBooks(); }
	
	@PostMapping("/book")
	public Book saveBook(@RequestBody Book book) { return bookService.save(book); }

	@DeleteMapping("/book/{id}")
	public void deleteBook(@PathVariable(value = "id") String id) { bookService.deleteBook(id); }
	
	@GetMapping("/book/{id}")
	public Book getBookById(@PathVariable(value = "id") String id) { return bookService.getBookById(id); }

	@GetMapping("/publisher/{name}")
	public List<Publisher> getPublisherByName(@PathVariable(value = "name") String name) { return publisherService.getPublisherByName(name); }

	@GetMapping("/publisher/book/{bookId}")
	public List<Publisher> getPublisherByBook(@PathVariable(value = "bookId") String bookId) { return publisherService.getPublisherByBook(bookId); }
}