package com.avenudecode.library.service.impl;

import com.avenudecode.library.entity.Book;
import com.avenudecode.library.repository.BookRepository;
import com.avenudecode.library.service.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(propagation = Propagation.SUPPORTS)
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {

    @Autowired
    private BookRepository bookRepository;

    @Override
    public List<Book> getAllBooks() { return bookRepository.findAll(); }

    @Override
    public Book save(Book book) {
        return bookRepository.save(book);
    }

    @Override
    public Book getBookById(String id) {
        return bookRepository.findById(id).orElseThrow(
                () -> new RuntimeException(String.format("Não foi possivel encontrar o livro")));
    }

    @Override
    public void deleteBook(String id) {
        Book deleteBook = bookRepository.findById(id).orElseThrow(
                () -> new RuntimeException(String.format("Não foi possivel encontrar o livro")));
        bookRepository.delete(deleteBook);
    }

}
