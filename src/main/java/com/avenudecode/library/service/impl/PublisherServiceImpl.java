package com.avenudecode.library.service.impl;

import com.avenudecode.library.entity.Publisher;
import com.avenudecode.library.repository.PublisherRepository;
import com.avenudecode.library.service.PublisherService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(propagation = Propagation.SUPPORTS)
@RequiredArgsConstructor
public class PublisherServiceImpl implements PublisherService {

    @Autowired
    private PublisherRepository publisherRepository;

    @Override
    public List<Publisher> getPublisherByName(String name) { return publisherRepository.getPublisherByName(name); }

    @Override
    public List<Publisher> getPublisherByBook (String id) { return publisherRepository.getPublisherByBook(id); }

}
