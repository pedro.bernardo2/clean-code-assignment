package com.avenudecode.library.service;

import com.avenudecode.library.entity.Book;

import java.util.List;

public interface BookService {

    List<Book> getAllBooks();

    Book save(Book book);

    Book getBookById(String id);

    void deleteBook(String id);

}
