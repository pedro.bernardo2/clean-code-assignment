package com.avenudecode.library.service;

import com.avenudecode.library.entity.Publisher;

import java.util.List;

public interface PublisherService {

    List<Publisher> getPublisherByName(String name);

    List<Publisher> getPublisherByBook(String id);

}
