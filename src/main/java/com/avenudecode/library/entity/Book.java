package com.avenudecode.library.entity;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Entity
@Table(name = "TB_BOOK")
public class Book {
	
	@Id
	private String id;

	@Column
	private String title;

	@Column
	private String author;

	@Column
	private Boolean rented;

	@Column
	private BigDecimal value;

	@Column
	private Date date;

	@OneToOne
	private Publisher publisher;

}
