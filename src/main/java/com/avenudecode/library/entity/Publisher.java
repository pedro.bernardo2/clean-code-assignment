package com.avenudecode.library.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Entity
@Table(name = "TB_PUBLISHER")
public class Publisher {

    @Id
    private String id;

    @Column
    private String name;

    @Column
    private Date date;

}
